import { Injectable } from '@angular/core';
import { Clase } from '../models/clase';
import { Observable, map, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClaseService {

  private clases: Clase[] = [
    {
      id: 1,
      nombre: "Matematicas",
      creditos: 5,
      profesor: "Juancho"
    },
    {
      id: 2,
      nombre: "Lenguaje",
      creditos: 3,
      profesor: "Pamela Anderson"
    },
    {
      id: 3,
      nombre: "Sociales",
      creditos: 3,
      profesor: "Aldair"
    }
  ];

  private url: string = 'http://localhost:8080/api/v1/clases'; 

  constructor(private http: HttpClient){
  }

  findAll(): Observable<Clase[]>{
    //return of(this.clases);
    return this.http.get<Clase[]>(this.url).pipe(
      map((response: any) => response as Clase[]),
    );
  }

  create(clase: Clase):Observable<Clase>{
    return this.http.post<Clase>(this.url, clase);
  }

  update(clase: Clase):Observable<Clase>{
    return this.http.put<Clase>(`${this.url}/${clase.id}`, clase);
  }
}
