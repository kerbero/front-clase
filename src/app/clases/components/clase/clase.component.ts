import { Component, OnInit } from '@angular/core';
import { Clase } from '../../models/clase';
import { ClaseService } from '../../services/clase.service';
import { FormComponent } from '../form/form.component';

@Component({
  selector: 'app-clase',
  standalone: true,
  imports: [FormComponent],
  templateUrl: './clase.component.html',
  styleUrl: './clase.component.css'
})
export class ClaseComponent implements OnInit{


  clases: Clase[] = [];
  claseSelected: Clase = new Clase();
  constructor(private service: ClaseService){}
  
  ngOnInit(): void {
    this.service.findAll().subscribe(clases => this.clases = clases);
  }

  addClase(clase: Clase) {
    if(clase.id > 0){
      this.service.create(clase).subscribe(claseUpdate => {
        this.clases = this.clases.map( cla => {
          if(cla.id == clase.id){
            return { ...claseUpdate};
          }
          return cla;
        });
      });
      
    }else{
    //clase.id = new Date().getTime();
    //this.clases.push(clase);
    this.service.create(clase).subscribe(claseNew => {
      this.clases = [... this.clases, { ...claseNew }];
    });
    }
    this.claseSelected = new Clase();
  }

  onUpdateClase(claseRow: Clase) {
    this.claseSelected = {... claseRow};  
  }
}
