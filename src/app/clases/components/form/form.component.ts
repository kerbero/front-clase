import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Clase } from '../../models/clase';
import { FormsModule, NgForm } from '@angular/forms';
import { ClaseService } from '../../services/clase.service';

@Component({
  selector: 'app-form',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './form.component.html',
  styleUrl: './form.component.css'
})
export class FormComponent {

@Input() clase: Clase = {
    id: 0,
    nombre: "",
    creditos: 0,
    profesor:""
  };
@Output() newClase = new EventEmitter();

constructor(private service: ClaseService){}

  onSubmit(claseForm: NgForm):void {
    this.newClase.emit(this.clase);
    console.log(this.clase);
    claseForm.reset();
    claseForm.resetForm();
  }

  clean() {
    this.clase = {
      id: 0,
      nombre: "",
      creditos: 0,
      profesor:""
    };
  }
}
