import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ClaseComponent } from './clases/components/clase/clase.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, ClaseComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Clase de angular para DAM2';

  enabled = false;
  clases: string[]= ["DAM1","DAM2","DAM3"];
  setEnable():void{
    this.enabled = this.enabled? false: true;
  }
}
